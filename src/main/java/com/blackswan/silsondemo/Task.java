package com.blackswan.silsondemo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@SuppressWarnings({"WeakerAccess", "unused"})
public class Task {

    public static final String STATUS_TODO        = "todo";
    public static final String STATUS_IN_PROGRESS = "in_progress";
    public static final String STATUS_DONE        = "done";

    @Id @GeneratedValue(strategy = GenerationType.AUTO) private Long   id;
                                                        private String name;
                                                        private String description;
                                                        private Date   dateCreated;
    @DateTimeFormat(iso=DateTimeFormat.ISO.DATE)        private Date   dateDue;
    @ManyToOne @JoinColumn(name="user_id") @JsonIgnore  private User   user;
                                                        private String status = STATUS_TODO;

    public Task() { super(); }

    public Task(String name,
                String description,
                Date   dateCreated,
                Date   dateDue) {
        super();
        this.name        = name;
        this.description = description;
        this.dateCreated = dateCreated;
        this.dateDue     = dateDue;
        this.status      = STATUS_TODO;
    }

    // Getters
    public Long   getId()          { return id;          }
    public String getName()        { return name;        }
    public String getDescription() { return description; }
    public Date   getDateCreated() { return dateCreated; }
    public User   getUser()        { return user;        }
    public Date   getDateDue()     { return dateDue;     }
    public String getStatus()      { return status;      }

    // Setters
    public void setId         ( Long   id          ) { this.id          = id;          }
    public void setName       ( String name        ) { this.name        = name;        }
    public void setDescription( String description ) { this.description = description; }
    public void setDateCreated( Date   dateCreated ) { this.dateCreated = dateCreated; }
    public void setUser       ( User   user        ) { this.user        = user;        }
    public void setDateDue    ( Date   dateDue     ) { this.dateDue     = dateDue;     }
    public void setStatus     ( String status      ) { this.status      = status;      }

    @Override
    public String toString() {
        return "Task{" +
                "  id="           + id          +
                ", name='"        + name        + '\'' +
                ", description='" + description + '\'' +
                ", dateCreated="  + dateCreated +
                '}';
    }

}
