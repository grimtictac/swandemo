package com.blackswan.silsondemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ResetController {
    private final UserRepository userRepository;
    private final TaskRepository taskRepository;

    @Autowired
    ResetController( UserRepository userRepository,
                        TaskRepository taskRepository )
    {
        this.userRepository = userRepository;
        this.taskRepository = taskRepository;
    }

    @RequestMapping( value    = "/api/reset",
            method   = RequestMethod.DELETE,
            produces = "application/json" )
    void reset()
    {
        taskRepository.deleteAll();
        userRepository.deleteAll();
    }

}
