package com.blackswan.silsondemo;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SilsondemoApplication {

	public static void main(String[] args) {
	    SpringApplication.run(SilsondemoApplication.class, args);
	}
}
