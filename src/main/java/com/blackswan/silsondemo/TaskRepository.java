package com.blackswan.silsondemo;

import org.springframework.data.repository.CrudRepository;

@SuppressWarnings("WeakerAccess")
public interface TaskRepository extends CrudRepository<Task, Long> { }
