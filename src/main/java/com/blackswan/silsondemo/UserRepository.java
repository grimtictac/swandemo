package com.blackswan.silsondemo;

import org.springframework.data.repository.CrudRepository;

@SuppressWarnings("WeakerAccess")
public interface UserRepository extends CrudRepository<User, Long> { }
