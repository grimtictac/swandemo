package com.blackswan.silsondemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@RestController
@SuppressWarnings("unused")
class UserRestController
{

    private final UserRepository userRepository;
    private final TaskRepository taskRepository;

    @Autowired
    UserRestController( UserRepository userRepository,
                        TaskRepository taskRepository )
    {
        this.userRepository = userRepository;
        this.taskRepository = taskRepository;
    }

    @RequestMapping( value    = "/api/user/{userId}/task",
                     method   = RequestMethod.GET,
                     produces = "application/json" )
    Collection<Task> getTasks(@PathVariable String userId)
    {
        User u = userRepository.findById(Long.parseLong(userId)).
                 orElseThrow(()->new EntityNotFoundException("User not found:" + userId));

        return u.getTasks();
    }

    @RequestMapping( value    = "/api/user/{userId}/task",
                     method   = RequestMethod.POST,
                     produces = "application/json" )
    Task addTask( @PathVariable String userId,
                  @RequestBody  Task t )
    {
        User u = userRepository.findById(Long.parseLong(userId)).
                orElseThrow(()->new EntityNotFoundException("User not found:" + userId));

        /* Behold the glory of Date creation the non-deprecated way! */
        t.setDateCreated(Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant()));

        u.addTask(t);

            userRepository.save(u);
        t = taskRepository.save(t);

        return t;
    }

}