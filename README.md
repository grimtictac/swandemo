# SwanDemo
Please see the *updated* video: 
https://goo.gl/oKfvVB
(now with more features!)

### Published and documented Postman Collection:
https://documenter.getpostman.com/view/4459196/swan/RW8CHTWk

### Quick Link:
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/1ea623cbafe63b5be639)

You can either clone this repo and run it locally using the Postman collection and the "local" environment.
Or, for added convenience, you can run against the Azure instance by selecting the "Azure" environment.

### Notes:

* It contains no logging and not authorization. For a production environment I would add those. I just didn't due to time constraints. Also the error handling is extremely basic, it pretty much only covers happy path and user not found. I am sure there are tons of other failure cases, but I was focussing more on getting development framework stuff in place. The current state of the code is such that I'd be okay to hand it over to somebody to harden more fuilly.

* I've tweaked the API enpoints slightly from the spec. Specifically I've pluralized the "tasks" and "users" GET endpoints. This conforms better with what Spring Data provides out the box.

* I was not able to remove all the HATEOAS related "_links" and "_embedded" stuff that Spring Data provides. I find it a bit more over-engineered than this problem requires, but there is no way to turn it off.

* As mentioned in the video, there is an issue with ID sequences not resetting, so if you keep creating and deleting users you'll have to keep changing the user_id environment variable. There is a bramch trying to address that problem but it was getting too complicated.